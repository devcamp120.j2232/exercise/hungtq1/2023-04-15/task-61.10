package com.devcamp.task61_10.jackson_json;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JacksonJsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(JacksonJsonApplication.class, args);
	}

}
