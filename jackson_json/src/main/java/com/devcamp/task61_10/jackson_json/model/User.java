package com.devcamp.task61_10.jackson_json.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class User {
    private int id;
	private String name;
	//@JsonManagedReference
    @JsonIgnore
	private List<Item> userItems;

	public User(int id, String name) {
		this.id = id;
		this.name = name;
		userItems = new ArrayList<>();
	}
	public void addItem(Item item) {
		this.userItems.add(item);
	}

    public User() {
    }
    public User(int id, String name, List<Item> userItems) {
        this.id = id;
        this.name = name;
        this.userItems = userItems;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public List<Item> getUserItems() {
        return userItems;
    }
    public void setUserItems(List<Item> userItems) {
        this.userItems = userItems;
    }

    

    
}
